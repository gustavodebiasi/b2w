import json
from flask import request
from model.swapi import SwapiModel
from flask_restful import Resource
from model.planeta import PlanetaModel
from marshmallow import Schema, fields, ValidationError, validate

class PlanetaSchema(Schema):
    nome = fields.String(required=True, validate=[
        validate.Length(min=1, max=100)
    ])
    clima = fields.String(required=True, validate=[
        validate.Length(min=1, max=100)
    ])
    terreno = fields.String(required=True, validate=[
        validate.Length(min=1, max=100)
    ])

class IdSchema(Schema):
    id = fields.Integer(required=True)

class PlanetaApi(Resource):
    def get(self, planeta_id=None, nome=None):
        if (planeta_id):
            planeta = PlanetaModel.objects(id=planeta_id).first()
            if (planeta == None):
                return {"error": 'Planeta not found'}, 404

            return json.loads(planeta.to_json())
        elif (nome):
            planeta = PlanetaModel.objects(nome=nome).first()
            if (planeta == None):
                return {"error": 'Planeta not found'}, 404
                
            return json.loads(planeta.to_json())
        else:
            return json.loads(PlanetaModel.objects.all().to_json())

    def post(self):
        try:
            result = PlanetaSchema().load(request.form)
        except ValidationError as err:
            return err.messages, 422

        planet = PlanetaModel()
        planet.nome = result['nome']
        planet.clima = result['clima']
        planet.terreno = result['terreno']
        swapi = SwapiModel.objects(nome=result['nome']).first()
        if (swapi):
            planet.quantidade_filmes = swapi.quantidade_filmes

        planet.save()

        return {'data': json.loads(planet.to_json())}, 201

    def delete(self):
        try:
            result = IdSchema().load(request.form)
        except ValidationError as err:
            return err.messages, 422
        
        planeta = PlanetaModel.objects(id=result['id']).first()
        if (not planeta):
            return {"error": 'Planeta not found'}, 404

        planeta.delete()
        planeta.save()

        return {"data": 'Planeta deleted'}



    
