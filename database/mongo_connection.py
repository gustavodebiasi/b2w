import sys, os
sys.path.insert(0, os.path.dirname(os.path.abspath(__file__)) + '/../')
import mongoengine
from os import getenv


class MongoConnection(object):

    def connect(self):
        mongoengine.connect(
            host=getenv('MONGO_DB_HOST'),
        )