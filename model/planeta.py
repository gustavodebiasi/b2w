import mongoengine

class PlanetaModel(mongoengine.Document):
    exclude = ['id']

    id = mongoengine.SequenceField(primary_key=True)
    nome = mongoengine.StringField(max_length=100, required=True)
    clima = mongoengine.StringField(max_length=100, required=True)
    terreno = mongoengine.StringField(max_length=100, required=True)
    quantidade_filmes = mongoengine.IntField(default=0)
    