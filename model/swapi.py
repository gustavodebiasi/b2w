import mongoengine

class SwapiModel(mongoengine.Document):
    quantidade_filmes = mongoengine.IntField(default=0)
    nome = mongoengine.StringField(max_length=100, required=True)
    