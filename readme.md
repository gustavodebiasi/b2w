## API Star Wars

Essa API foi criada como forma de validação técnica para processo seletivo na empresa B2W.

## Instalação

- Instale o Python3 (preferencialmente o 3.8)
- Configure o seu banco de dados MongoDB (ou utilize a configuração disponibilizada no .env.example)
- Instale as dependências:
```
pip install -r requirements.txt
```

## Configuração

- Configure o seu .env (no repositório há um .env.example para você se familiarizar com as variáveis necessárias)
- Configure no seu agendador de tarefas ou seu cron job para executar o swapi_service.py para atualização de quantidade de filmes dos planetas.

## Rodar o servidor da API

```
python3 run.py
```

## Testes Unitários

Antes de rodar os testes, configure o pytest.ini com as suas configurações de variáveis de ambiente.

```
pytest
```

## Utilização

No arquivo API Star Wars.postman_collection.json há uma coleção do postman para realizar a utilização da API. De igual forma, abaixo os endpoints são descritos.

- **GET** - **/api/planetas/**: Retornando a lista de planetas disponíveis e seus atributos

- **GET** - **/api/planetas/:id**: Utilizando o query string com o id como parâmetro a API irá buscar e retornar o planeta referente ao ID.

- **GET** - **/api/planetas/:nome**: Utilizando o query string com o nome do planeta como parâmetro a API irá buscar e retornar o planeta referente ao Nome.

- **POST** - **/api/planetas/**: Para criar um novo planeta. Neste endpoint é obrigatório o envio das seguintes informações no corpo da requisição: nome, terreno e clima.

- **DELETE** - **/api/planetas/**: Para deletar um planeta. Neste endpoint é obrigatório o envio das seguintes informações no corpo da requisição: id.
