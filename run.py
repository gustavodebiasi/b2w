from os import getenv
from flask import Flask
from flask_restful import Api
from api.planeta import PlanetaApi
from database.mongo_connection import MongoConnection

app = Flask(__name__)
api = Api(app)

# Routes
base_api_url = '/api/planetas/'

api.add_resource(PlanetaApi, 
    base_api_url + '/',
    base_api_url + '/<int:planeta_id>',
    base_api_url + '/<string:nome>'
)

mongo = MongoConnection()
mongo.connect()

if __name__ == '__main__':
    host = getenv('HOST')
    debug = getenv('DEBUG')
    port = getenv('PORT')
    app.run(host=host,port=port,debug=debug)
