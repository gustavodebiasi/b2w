import json
import requests
from os import getenv
from model.swapi import SwapiModel
from model.planeta import PlanetaModel

class SwapiService(object):
    base_url = 'https://swapi.co/'
    __quantidade__ = 0

    def executar(self):
        url = self.base_url + '/api/planets/'
        self.__buscar__(url)

    def __buscar__(self, url):
        response = requests.get(url)
        if (response.status_code != 200):
            raise Exception('Não foi possível obter os dados do webservice.')

        dados = response.json()
        self.__atualizar_dados__(dados['results'])
        
        if (dados['count'] > self.__quantidade__):
            return self.__buscar__(dados['next'])

    def __atualizar_dados__(self, results):
        for result in results:
            quantidade_filmes = len(result['films'])
            nome = result['name']
            self.__cadastrar_planetas_swapi__(nome, quantidade_filmes)
            self.__atualizar_planeta__(nome, quantidade_filmes)
            self.__quantidade__ += 1

    def __cadastrar_planetas_swapi__(self, nome, quantidade_filmes):
        SwapiModel.objects(nome=nome).update(quantidade_filmes=quantidade_filmes, upsert=True)

    def __atualizar_planeta__(self, nome, quantidade_filmes):
        PlanetaModel.objects(nome=nome).modify(quantidade_filmes=quantidade_filmes)

from swapi_service import SwapiService
from database.mongo_connection import MongoConnection

if __name__ == '__main__':
    mongo = MongoConnection()
    mongo.connect()
    swap = SwapiService()
    swap.executar()