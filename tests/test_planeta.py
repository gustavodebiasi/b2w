import sys, os
sys.path.insert(0, os.path.dirname(os.path.abspath(__file__)) + '/../')

import json
import unittest
from run import app
from model.planeta import PlanetaModel
from mongoengine.errors import ValidationError

class TestPlaneta(unittest.TestCase):
    def setUp(self):
        self.app = app.test_client()
        PlanetaModel.objects.all().delete()
        

    def cadastrar_planeta(self, dados_testes_completos):
        return self.app.post(
            '/api/planetas/', 
            method='POST', 
            data=dict(dados_testes_completos)
        )

    def dados_testes_completos(self):
        return {
            'nome': 'Alderaan',
            'terreno': 'planicie',
            'clima': 'seco'
        }

    def test_cadastrar_planeta_com_sucesso(self):
        dados_testes = self.dados_testes_completos()
        response = self.cadastrar_planeta(dados_testes)

        self.assertEqual(201, response.status_code)
        json_data = json.loads(response.data)['data']
        assert '_id' in json_data
        assert 'clima' in json_data
        self.assertEqual(dados_testes['clima'], json_data['clima'])
        assert 'nome' in json_data
        self.assertEqual(dados_testes['nome'], json_data['nome'])
        assert 'terreno' in json_data
        self.assertEqual(dados_testes['terreno'], json_data['terreno'])
        assert 'quantidade_filmes' in json_data

    def test_cadastrar_planeta_falha_validacao(self):
        dados_testes = {
            'name': 'teste',
            'terre': 'terra',
            'clim': 'clima'
        }
        response = self.cadastrar_planeta(dados_testes)

        self.assertEqual(422, response.status_code)
    
    def test_get_todos_planetas(self):
        dados_completos = self.dados_testes_completos()
        self.cadastrar_planeta(dados_completos)
        response = self.app.get('/api/planetas/')

        self.assertEqual(200, response.status_code)
        json_data = json.loads(response.data)[0]
        assert '_id' in json_data
        assert 'clima' in json_data
        self.assertEqual(dados_completos['clima'], json_data['clima'])
        assert 'nome' in json_data
        self.assertEqual(dados_completos['nome'], json_data['nome'])
        assert 'terreno' in json_data
        self.assertEqual(dados_completos['terreno'], json_data['terreno'])
        assert 'quantidade_filmes' in json_data

    def test_get_planeta_por_nome(self):
        dados_completos = self.dados_testes_completos()
        self.cadastrar_planeta(dados_completos)
        response = self.app.get('/api/planetas/' + dados_completos['nome'])

        self.assertEqual(200, response.status_code)
        json_data = json.loads(response.data)
        assert '_id' in json_data
        assert 'clima' in json_data
        self.assertEqual(dados_completos['clima'], json_data['clima'])
        assert 'nome' in json_data
        self.assertEqual(dados_completos['nome'], json_data['nome'])
        assert 'terreno' in json_data
        self.assertEqual(dados_completos['terreno'], json_data['terreno'])
        assert 'quantidade_filmes' in json_data

    def test_get_planeta_por_nome_not_found(self):
        response = self.app.get('/api/planetas/gustavo')

        self.assertEqual(404, response.status_code)

    def test_get_planeta_por_id(self):
        dados_completos = self.dados_testes_completos()
        resposta_cadastro = self.cadastrar_planeta(dados_completos)
        json_data = json.loads(resposta_cadastro.data)['data']
        response = self.app.get('/api/planetas/' + str(json_data['_id']))

        self.assertEqual(200, response.status_code)
        json_data = json.loads(response.data)
        assert '_id' in json_data
        assert 'clima' in json_data
        self.assertEqual(dados_completos['clima'], json_data['clima'])
        assert 'nome' in json_data
        self.assertEqual(dados_completos['nome'], json_data['nome'])
        assert 'terreno' in json_data
        self.assertEqual(dados_completos['terreno'], json_data['terreno'])
        assert 'quantidade_filmes' in json_data

    def test_get_planeta_por_id_not_found(self):
        response = self.app.get('/api/planetas/1')

        self.assertEqual(404, response.status_code)

    def test_delete_planeta(self):
        dados_completos = self.dados_testes_completos()
        resposta_cadastro = self.cadastrar_planeta(dados_completos)
        json_data = json.loads(resposta_cadastro.data)['data']
        id = json_data['_id']
        response = self.app.delete(
            '/api/planetas/', 
            method='DELETE', 
            data=dict(id=id)
        )

        self.assertEqual(200, response.status_code)

    def test_delete_planeta_not_found(self):
        response = self.app.delete(
            '/api/planetas/', 
            method='DELETE', 
            data=dict(id=4)
        )

        self.assertEqual(404, response.status_code)

    def test_delete_planeta_falha_validacao(self):
        response = self.app.delete(
            '/api/planetas/', 
            method='DELETE', 
            data=dict()
        )

        self.assertEqual(422, response.status_code)
        

if __name__ == '__main__':
    unittest.main()
